<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");

$arComponentParameters   = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        'OBJECT_COMMENT_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage("OBJECT_COMMENT_TYPE"),
            'TYPE' => 'LIST',
            'VALUES' => array(4 => 'Элемент инфоблока', 5 => 'Страница'),
        ),
        "OBJECT_COMMENT" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("OBJECT_COMMENT"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    )
);