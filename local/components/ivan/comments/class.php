<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable;
Loader::includeModule("highloadblock");

/**
 * Class Comments
 */
class Comments extends CBitrixComponent
{
    /**
     * Собираем комментарии из справочника комментариев
     *
     * @param $objComType
     * @param $objCom
     * @return array
     */
    public function selectVal($objComType, $objCom)
    {
        $arr = [];
        $arHLBlock = HighloadBlockTable::getById(HL_COMMENTS)->fetch();
        $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $rsData = $strEntityDataClass::getList([
            'select' => ['ID', 'UF_DATE_TIME', 'UF_USER_NAME', 'UF_COMMENT_TEXT', 'UF_PARENT_COMMENT_ID', 'UF_OBJECT_COMMENT_TYPE', 'UF_OBJECT_COMMENT'],
            'filter' => ['UF_OBJECT_COMMENT_TYPE' => $objComType, 'UF_OBJECT_COMMENT' => $objCom],
            'order' => ['ID' => 'DESC']
        ]);
        while ($arItem = $rsData->Fetch()) {
            array_push($arr, $arItem);
        }
        return $arr;
    }

    /**
     * Строим дерево комментариев
     *
     * @param $tree
     * @param $parentId
     * @param $res
     */
    public function getTree($tree, $parentId, &$res)
    {
        foreach ($tree as $hiElem) {
            if ($hiElem['UF_PARENT_COMMENT_ID'] == $parentId) {
                array_push($res, $hiElem);
                $this->getTree($tree, $hiElem["ID"],$res);
            }
        }
    }

    /**
     * Добавляем комментариям уровни вложенности
     *
     * @param $tree
     * @return mixed
     */
    public function addDepthLevel($tree)
    {
        $hiElemIdPred = -1; $hiElemParIdPredPred = -1; $levelIn = 0;
        foreach ($tree as &$hiElem) {
            if ($hiElem['UF_PARENT_COMMENT_ID'] != null) {
                if ($hiElem['UF_PARENT_COMMENT_ID'] == $hiElemIdPred)
                    $levelIn++;
                else if ($hiElem['UF_PARENT_COMMENT_ID'] != $hiElemParIdPredPred)
                    $levelIn--;
            }
            else $levelIn = 0;
            $hiElem['DEPTH_LEVEL'] = $levelIn;
            $hiElemParIdPredPred = $hiElem['UF_PARENT_COMMENT_ID'];
            $hiElemIdPred = $hiElem["ID"];
        }
        return $tree;
    }

    /**
     * Добавляем новый комментарий в справочник комментариев
     *
     * @param $request
     */
    public function addElement($request)
    {
        $dateVal = date("d.m.Y H:i:s");
        $fio = $request->getPost("fio");
        $commentText = $request->getPost("commentText");
        $parent = $request->getPost("parent");
        $objCommType = $request->getPost("objCommType");
        $objComm = $request->getPost("objComm");
        $arHLBlock = HighloadBlockTable::getById(HL_COMMENTS)->fetch();
        $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $dataInsert = [
            'UF_DATE_TIME' => $dateVal,
            'UF_USER_NAME' => $fio,
            'UF_COMMENT_TEXT' => $commentText,
            'UF_PARENT_COMMENT_ID' => $parent,
            'UF_OBJECT_COMMENT_TYPE' => $objCommType,
            'UF_OBJECT_COMMENT' => $objComm
        ];
        $strEntityDataClass::add($dataInsert);
    }

    /**
     *
     */
    public function executeComponent()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        if($request->isAjaxRequest()){
            $this->addElement($request);
        }
        $res = [];
        $this->arResult["HI_ELEM"] = $this->selectVal($this->arParams["OBJECT_COMMENT_TYPE"], $this->arParams["OBJECT_COMMENT"]);
        $this->getTree($this->arResult["HI_ELEM"], null,$res);
        $this->arResult["TREE"] = $res;
        $this->arResult["TREE"] = $this->addDepthLevel($this->arResult["TREE"]);
        $this->includeComponentTemplate();
    }
}