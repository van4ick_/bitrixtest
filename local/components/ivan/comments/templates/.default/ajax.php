<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;

$request = Application::getInstance()->getContext()->getRequest();
if($request->isAjaxRequest() && check_bitrix_sessid()) {
    $objCommType = $request->getPost("objCommType");
    $objComm = $request->getPost("objComm");

    $APPLICATION->IncludeComponent(
        "ivan:comments",
        "",
        [
            "OBJECT_COMMENT_TYPE" => $objCommType,
            "OBJECT_COMMENT" => $objComm
        ]
    );
}
