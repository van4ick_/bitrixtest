var oneComment = null;
$(function() {
    $('body').on('click', '#reviews .comment-react-button', function() {
        $(this).parent().find('.react_comment').css('display','flex');
            $(this).hide();
            oneComment = $(this).parent().parent().attr("id");
    });
    $('body').on("click", "#reviews .react_comment_button", function(e) {
        e.preventDefault();
        var parentBlock = $(this).parent().parent();
        $data = {
            fio: '',
            commentText: '',
            parent: null,
            objCommType: parentBlock.attr('data-obj-comm-type'),
            objComm: parentBlock.attr('data-obj-comm'),
            sessid: null
        };
        if (parentBlock.attr('data-parent')!="null") $data.parent = parentBlock.attr('data-parent');
        var data = parentBlock.serializeArray();
        $.each(data, function() {
            switch (this.name) {
                case 'fio':
                    $data.fio = this.value;
                    break;
                case 'comment_text':
                    $data.commentText = this.value;
                    break;
                case 'sessid':
                    $data.sessid = this.value;
                    break;
            }
        });
        $.ajax({
            type: "POST",
            url: '/local/components/ivan/comments/templates/.default/ajax.php',
            data: $data,
            dataType: 'html',
            success: function (data) {
                $("#reviews").replaceWith(data);
            }
        });
    });
});
$(document).mouseup(function (e) {
    if (!$("#"+oneComment + " .react_comment").is(e.target)
        && $("#"+oneComment + " .react_comment").has(e.target).length === 0){
        $("#"+oneComment).find('.comment-react-button').show();
        $("#"+oneComment).find('.react_comment').hide();
    }
});