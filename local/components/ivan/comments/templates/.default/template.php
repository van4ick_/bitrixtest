<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $USER;?>

<div id="reviews">
    <form id="add_comment"  class="comment_react_form" data-parent="null" data-obj-comm-type="<?=$arParams["OBJECT_COMMENT_TYPE"]?>" data-obj-comm="<?=$arParams["OBJECT_COMMENT"]?>">
        <?=bitrix_sessid_post()?>
        <div class="title">Оставить отзыв</div>
        <?php if (!$USER->IsAuthorized()):?><label>ФИО: <input type="text" name="fio" required></label>
        <?php else:?><input type="hidden" name="fio" value="<?=$USER->GetFullName();?>"><?php endif;?>
        <label>Комментарий: <textarea name="comment_text" required></textarea></label>
        <div class="submit_input"><input type="submit" id="add_comment_button" class="react_comment_button" name="button" value="Отправить"></div>
    </form>
    <div id="comments">
        <div class="title">Отзывы</div>
        <?php if(!empty($arResult["TREE"])):?>
            <?php foreach($arResult["TREE"] as $hiElem):?>
                <?php if($hiElem['UF_PARENT_COMMENT_ID'] != null):?><div id="comment_<?=$hiElem["ID"];?>" class="comment comment_parent_not_null" style="margin-left: <?=$hiElem["DEPTH_LEVEL"]*40?>px; width: calc(100% - <?=$hiElem["DEPTH_LEVEL"]*40?>px);">
                <?php else:?><div id="comment_<?=$hiElem["ID"];?>" class="comment comment_parent_null"><?php endif;?>
                    <div class="comment-fio-date">
                        <i class="fas fa-level-down-alt"></i>
                        <div class="comment-fio"><?=$hiElem["UF_USER_NAME"];?></div>
                        <div class="comment-date"><?=$hiElem["UF_DATE_TIME"];?></div>
                    </div>
                    <div class="comment-text"><?=$hiElem["UF_COMMENT_TEXT"];?></div>
                    <div class="comment-react">
                        <div class="comment-react-button">Ответить</div>
                        <form class="react_comment comment_react_form" data-parent="<?=$hiElem["ID"];?>" data-obj-comm-type="<?=$arParams["OBJECT_COMMENT_TYPE"];?>" data-obj-comm="<?=$arParams["OBJECT_COMMENT"];?>">
                            <?=bitrix_sessid_post()?>
                            <?php if(!$USER->isAuthorized()):?><label>ФИО: <input type="text" name="fio" required></label>
                            <?php else:?><input type="hidden" name="fio" value="<?=$USER->GetFullName();?>"><?php endif;?>
                            <label>Комментарий: <textarea name="comment_text" required></textarea></label>
                            <div class="submit_input">
                                <input type="submit" class="react_comment_button" name="button" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>